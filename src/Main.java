import java.awt.Dimension;

/**
 *
 * @author Felipe Borges
 *
 */

public class Main {
    public static void main(String args[]) {
        mainWindow window = new mainWindow();
        window.setVisible(true);
        window.setResizable(true);
        window.setMinimumSize(new Dimension(1000, 300));
        window.setRef(window);
    }
}
