package backend;

import java.util.Date;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

import java.lang.Math;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.InputStreamReader;
import java.io.FileInputStream;
import java.io.File;

import static java.lang.System.out;

import net.sf.junidecode.Junidecode;

/**
 * 
 * Hidro files parser.
 *
 * @author Douglas Vanny Bento
 * @author Luciano
 *
 */

public class HidroParser implements ParserInterface
{
	public TableData parse(String content) throws ContentNotParseable
	{
		TableData table = new TableData();
		ArrayList<ArrayList<String>> lines = new ArrayList<ArrayList<String>>();
		ArrayList<String> columns = new ArrayList<String>();
		ArrayList<Class> classes = new ArrayList<Class>();
		int i=0;
		int j=0;
		
		String lines_vec[] = content.split("\n");
		
		String raw_header = "";
		
		boolean top_header = true;
		
		columns.add("Time Stamp");
		
		for(i=0; i<lines_vec.length; i++)
		{
			if(lines_vec[i].matches("-+"))
			{
				i++;
				
				break;
			}
			
			raw_header = raw_header + lines_vec[i] + "\n";
			
			if(i==0) continue;
			
			if(top_header)
			{
				if(lines_vec[i].length() == 0)
				{
					top_header = false;
					
					continue;
				}
				
				String last_field = null;
				
				for(String info : lines_vec[i].split(","))
				{
					String temp[] = info.split(": ",2);
					
					if(temp.length == 1)
					{
						temp = info.split(" = ",2);
					}
					
					if(temp.length == 1)
					{
						if(last_field == null)
						{
							throw new ContentNotParseable();
						}
						else
						{
							table.setInfoField(last_field,table.getInfoField(last_field,"") + ", " + temp[0].trim());
						}
					}
					else
					{
						if(temp.length != 2)
						{
							throw new ContentNotParseable();
						}
						
						last_field = Junidecode.unidecode(temp[0].trim());
						
						table.setInfoField(last_field,temp[1].trim());
					}
				}
			}
			else
			{
				if(lines_vec[i].length() == 0) continue;
				
				String temp[] = lines_vec[i].split(":",2);
				
				if(temp.length != 2)
				{
					throw new ContentNotParseable();
				}
				
				temp[0] = Junidecode.unidecode(temp[0].trim());
				
				if(temp[0].equals("Configuracao dos sensores")) continue;
				
				columns.add(temp[0]);
			}
		}
		
		table.setRawHeader(raw_header);
		
		for(j=0; j<2; j++)
		{
			for(; i<lines_vec.length; i++)
			{
				if(lines_vec[i].matches("-+"))
				{
					i++;
					
					break;
				}
			}
		}
		
		String sample_data[] = lines_vec[i].trim().split("\\s\\s+");
		
		if(sample_data.length != columns.size())
		{
			out.println(sample_data.length);
			out.println(columns.size());
			
			throw new ContentNotParseable();
		}
		
		for(j=0; j<sample_data.length; j++)
		{
			Class c = String.class;
			
			if(sample_data[j].matches("\\d+([\\.,]\\d+)?"))
			{
				c = Float.class;
			}
			else if(sample_data[j].matches("\\d\\d/\\d\\d/\\d\\d\\d\\d\\s\\d\\d:\\d\\d:\\d\\d"))
			{
				c = Date.class;
			}
			
			classes.add(c);
			
			String column_name = columns.get(j);
			
			try
			{
				table.addColumn(column_name,c);
			}
			catch(TableData.ColumnAlreadyExists e)
			{
				for(int k=2;;k++)
				{
					String neo_column_name = column_name + " " + k;
					
					try
					{
						table.addColumn(neo_column_name,c);
						
						break;
					}
					catch(TableData.ColumnAlreadyExists e2)
					{
					}
				}
			}
		}
		
		for(; i<lines_vec.length; i++)
		{
			ArrayList<Object> row = new ArrayList<Object>();
			String row_data[] = lines_vec[i].trim().split("\\s\\s+");
			
			if(row_data.length != columns.size())
			{
				throw new ContentNotParseable();
			}
			
			for(j=0; j<row_data.length; j++)
			{
				Object o = row_data[j];
				
				if(classes.get(j) == Float.class)
				{
					o = (Object) Float.parseFloat(((String)o).replace(",","."));
				}
				else if(classes.get(j) == Date.class)
				{
					String temp[] = ((String)o).split("\\s");
					String tempDate[];
					String tempTime[];
					
					tempDate = temp[0].split("/");
					tempTime = temp[1].split(":");
					
					Calendar c = Calendar.getInstance();
					c.set(
						new Integer(tempDate[2]),
						new Integer(tempDate[1])-1,
						new Integer(tempDate[0]),
						new Integer(tempTime[0]),
						new Integer(tempTime[1]),
						new Integer(tempTime[2])
					);
					
					o = (Object) c.getTime();
				}
				
				row.add(o);
			}
			
			try
			{
				table.addRow(row);
			}
			catch(TableData.TypeMismatch e)
			{
				throw new ContentNotParseable();
			}
			catch(TableData.RowLengthMismatch e)
			{
				throw new ContentNotParseable();
			}
		}
		
		return table;
	}
	
	public static void main(String[] args)
	{
		try
		{
			BufferedReader in = new BufferedReader(
				new InputStreamReader(
					new FileInputStream(
						new File("/media/Files/Dropbox_Ubuntu/Dropbox/UFPEL/Desenvolvimento de Software/parser/local/EH HS02_31102013_1650_COTA-086.txt")
					),
					"ISO-8859-1"
				)
			);
			
			String content = "";
			String line = null;
			
			while((line = in.readLine()) != null)
			{
				content = content + line + "\n";
			}
			
			TableData t = (new HidroParser()).parse(content);
			
			out.println(t);
		}
		catch(Exception e)
		{
			out.println(e);
		}
	}
}
	
