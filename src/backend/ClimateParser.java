package backend;

import java.util.Calendar;
import java.util.ArrayList;
import java.lang.Math;

import java.io.BufferedReader;
import java.io.FileReader;

import static java.lang.System.out;

/**
 * 
 * Climate files parser.
 *
 * @author Douglas Vanny Bento
 * @author Luciano
 *
 */

public class ClimateParser implements ParserInterface
{
	public TableData parse(String content) throws ContentNotParseable
	{
		TableData table = new TableData();
		ArrayList<ArrayList<String>> lines = new ArrayList<ArrayList<String>>();
		ArrayList<String> columns = new ArrayList<String>();
		ArrayList<Class> classes = new ArrayList<Class>();
		int i=0;
		int j=0;
		
		for(String s : content.split("\n"))
		{
			ArrayList<String> line = new ArrayList<String>();
			
			for(String s2 : s.split("\t"))
			{
				line.add(s2);
			}
			
			if(lines.size()>0 && line.size() != lines.get(0).size())
			{
				throw new ContentNotParseable();
			}
			
			lines.add(line);
		}
		
		if(lines.size()<3)
		{
			throw new ContentNotParseable();
		}
		
		for(i=0; i<lines.get(0).size(); i++)
		{
			String column_name;
			
			if(lines.get(0).get(i).length()>0)
			{
				column_name = lines.get(0).get(i).trim() + " " + lines.get(1).get(i).trim();
			}
			else
			{
				column_name = lines.get(1).get(i).trim();
			}
			
			Class c = String.class;
			
			if(lines.get(2).get(i).matches("\\d+([\\.,]\\d+)?"))
			{
				c = Float.class;
			}
			else if(lines.get(2).get(i).matches("\\d\\d/\\d\\d/\\d\\d"))
			{
				c = java.sql.Date.class;
			}
			else if(lines.get(2).get(i).matches("\\d\\d?:\\d\\d\\s[ap]"))
			{
				c = java.sql.Time.class;
			}
			
			columns.add(column_name);
			classes.add(c);
		}
		
		for(i=0; i<columns.size(); i++)
		{
			String column_name = columns.get(i);
			Class c = classes.get(i);
			
			if(i+1 < columns.size() && c == java.sql.Date.class && classes.get(i+1) == java.sql.Time.class)
			{
				column_name = column_name + " " + columns.get(i+1);
				c = java.util.Date.class;
				
				classes.remove(i);
				classes.set(i,c);
				
				columns.remove(i);
				columns.set(i,column_name);
			}
			
			try
			{
				table.addColumn(column_name,c);
			}
			catch(TableData.ColumnAlreadyExists e)
			{
				for(j=2;;j++)
				{
					String neo_column_name = column_name + " " + j;
					
					try
					{
						table.addColumn(neo_column_name,c);
						
						break;
					}
					catch(TableData.ColumnAlreadyExists e2)
					{
					}
				}
			}
		}
		
		for(i=2; i<lines.size(); i++)
		{
			ArrayList<Object> row = new ArrayList<Object>();
			
			int diff = 0;
			
			for(j=0; j<lines.get(i).size(); j++)
			{
				Object o = lines.get(i).get(j);
				Class c = classes.get(j + diff);
				
				if(c == Float.class)
				{
					o = (Object) Float.parseFloat(((String)o).replace(",","."));
				}
				else if(c == java.sql.Date.class)
				{
					String temp[] = ((String)o).split("/");
					
					o = (Object) new java.sql.Date(
						new Integer(temp[2]) + 2000 - 1900,
						new Integer(temp[1]) - 1,
						new Integer(temp[0])
					);
				}
				else if(c == java.sql.Time.class)
				{
					String temp[] = ((String)o).split("[\\s:]");
					int hour = new Integer(temp[0]);
					
					if(hour == 12 && temp[2].equals("a"))
					{
						hour = 0;
					}
					else if(hour < 12 && temp[2].equals("p"))
					{
						hour += 12;
					}
					
					o = (Object) new java.sql.Time(
						hour,
						new Integer(temp[1]),
						0
					);
				}
				else if(c == java.util.Date.class)
				{
					String tempDate[] = ((String)o).split("/");
					String tempTime[] = ((String)lines.get(i).get(j+1)).split("[\\s:]");
					
					int hour = new Integer(tempTime[0]);
					
					if(hour == 12 && tempTime[2].equals("a"))
					{
						hour = 0;
					}
					else if(hour < 12 && tempTime[2].equals("p"))
					{
						hour += 12;
					}
					
					Calendar cal = Calendar.getInstance();

					cal.set(
						new Integer(tempDate[2]) + 2000 - 1900,
						new Integer(tempDate[1]) - 1,
						new Integer(tempDate[0]),
						hour,
						new Integer(tempTime[1]),
						0
					);

					o = (Object) cal.getTime();
					
					j++;
					diff--;
				}
				
				row.add(o);
			}
			
			try
			{
				table.addRow(row);
			}
			catch(TableData.TypeMismatch e)
			{
				throw new ContentNotParseable();
			}
			catch(TableData.RowLengthMismatch e)
			{
				throw new ContentNotParseable();
			}
		}
		
		return table;
	}
	
	public static void main(String[] args)
	{
		try
		{
			BufferedReader in = new BufferedReader(new FileReader("/media/Files/Dropbox_Ubuntu/Dropbox/UFPEL/Desenvolvimento de Software/parser/local/EHM02_31102013.txt"));
			
			String content = "";
			String line = null;
			
			while((line = in.readLine()) != null)
			{
				content = content + line + "\n";
			}
			
			TableData t = (new ClimateParser()).parse(content);
			
			out.println(t);
		}
		catch(Exception e)
		{
			e.printStackTrace(System.out);
		}
	}
}
