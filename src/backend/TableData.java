package backend;

/**
 *
 * @author Douglas Vanny Bento
 *
 */

import java.lang.Object;
import java.lang.Cloneable;
import java.lang.Comparable;
import java.lang.Math;
import java.util.Comparator;
import java.util.Collections;
import java.util.ArrayList;
import java.util.HashMap;
import static java.lang.System.out;

/**
 * 
 * Class that stores a table of multi-type objects and info regarding the table.
 * <p>
 * In order to fill the table, first it is necessary to inform the columns this table has by calling the method {@link #addColumn}.
 * <p>
 * After that, rows should be inserted by calling {@link #addRow}.
 * <p>
 * Table content can be retreived by calling the methods {@link #getColumns}, {@link #getRow} and {@link #getRows}.
 * <p>
 * It is also possible to inform a raw header with the method {@link #setRawHeader} or preprocessed info with {@link #setInfoField}
 * <p>
 * Table manipulation can be done with {@link #sort} and {@link #filter}. Please check {@link FilterInterface} for more details regarding filters.
 *
 * @author Douglas Vanny Bento
 *
 */

public class TableData extends Object implements Cloneable
{
	/**
	 * 
	 * Exception thrown when someone tries to add a column that already exists.
	 *
	 */
	public class ColumnAlreadyExists extends Exception
	{
		String type;
		
		public ColumnAlreadyExists(String t)
		{
			super("Column %s already exists.".format(t));
			type = t;
		}
	}
	
	/**
	 * 
	 * Exception thrown when someone tries to access an column that does not exist.
	 *
	 */
	public class ColumnNotFound extends Exception
	{
		String type;
		
		public ColumnNotFound(String t)
		{
			super("Column %s not found.".format(t));
			type = t;
		}
	}
	
	/**
	 * 
	 * Exception thrown when someone tries to add a row with an object of the wrong class.
	 *
	 */
	public class TypeMismatch extends Exception
	{
		Class received;
		Class expected;
		
		public TypeMismatch(Class r, Class e)
		{
			super("Expected %s. Received %s.".format(e.toString(),r.toString()));
			received = r;
			expected = e;
		}
	}
	
	/**
	 * 
	 * Exception thrown when someone tries to add a row with the wrong number of fields.
	 *
	 */
	public class RowLengthMismatch extends Exception
	{
		Integer received;
		Integer expected;
		
		public RowLengthMismatch(Integer r, Integer e)
		{
			super("Expected %s values. Received %s.".format(e.toString(), r.toString()));
			received = r;
			expected = e;
		}
	}
	
	private class RowSorting implements Comparable
	{
		public ArrayList<Object> row;
		private int field;
		
		public RowSorting(ArrayList<Object> objects_row, int field_to_compare)
		{
			row = objects_row;
			field = field_to_compare;
		}
		
		public int compareTo(Object that)
		{
			ArrayList<Object> that_row = ((RowSorting)that).row;
			
			return ((Comparable)row.get(field)).compareTo(that_row.get(field));
		}
	}
	
	private ArrayList<ArrayList<Object>> content;
	private HashMap<String,Class> types;
	private ArrayList<String> column_order;
	
	private String rawHeader;
	
	private HashMap<String,String> info_fields;
	
	public TableData()
	{
		content = new ArrayList<ArrayList<Object>>();
		types = new HashMap<String,Class>();
		column_order = new ArrayList<String>();
		
		info_fields = new HashMap<String,String>();
		
		rawHeader = "";
	}
	
	public void addColumn(String column_name, Class column_type) throws ColumnAlreadyExists
	{
		if(types.containsKey(column_name))
		{
			throw new ColumnAlreadyExists(column_name);
		}
		
		types.put(column_name,column_type);
		column_order.add(column_name);
	}
	
	public ArrayList<String> getColumns()
	{
		return (ArrayList<String>) column_order.clone();
	}
	
	public HashMap<String,Class> getTypes()
	{
		return (HashMap<String,Class>) types.clone();
	}
	
	public void addRow(ArrayList<Object> row) throws RowLengthMismatch, TypeMismatch
	{
		if(row.size() != column_order.size())
		{
			throw new RowLengthMismatch(row.size(),column_order.size());
		}
		
		int pos = 0;
		
		for(pos=0; pos<row.size(); pos++)
		{
			Object item = row.get(pos);
			Class t = types.get(column_order.get(pos));
			
			if(!t.isInstance(item))
			{
				throw new TypeMismatch(item.getClass(),t);
			}
			else
			{
				row.set(pos,t.cast(item));
			}
		}
		
		content.add(row);
	}
	
	public ArrayList<Object> getRow(int pos)
	{
		if(pos<0)
		{
			pos = countRows()+pos;
		}
		
		return (ArrayList<Object>)content.get(pos).clone();
	}
	
	public ArrayList<ArrayList<Object>> getRows()
	{
		return getRows(0,countRows());
	}
	
	public ArrayList<ArrayList<Object>> getRows(Integer a)
	{
		return getRows(a,countRows());
	}
	
	public ArrayList<ArrayList<Object>> getRows(Integer a, Integer b)
	{
		if(a<0)
		{
			a = countRows()+a;
		}
		
		if(b<0)
		{
			b = countRows()+b;
		}
		
		ArrayList<ArrayList<Object>> ret;
		
		if(a<b)
		{
			ret = new ArrayList<ArrayList<Object>>(content.subList(a,b));
		}
		else
		{
			ret = new ArrayList<ArrayList<Object>>(content.subList(b,a));
			
			Collections.reverse(ret);
		}
		
		return ret;
	}
	
	public HashMap<String,Object> getRowAsHashMap(int pos)
	{
		if(pos<0)
		{
			pos = countRows()+pos;
		}
		
		ArrayList<Object> row = (ArrayList<Object>)content.get(pos).clone();
		HashMap<String,Object> ret = new HashMap<String,Object>();
		
		for(pos=0; pos<row.size(); pos++)
		{
			ret.put(column_order.get(pos),row.get(pos));
		}
		
		return ret;
	}
	
	public ArrayList<HashMap<String,Object>> getRowsAsHashMap()
	{
		return getRowsAsHashMap(0,countRows());
	}
	
	public ArrayList<HashMap<String,Object>> getRowsAsHashMap(Integer a)
	{
		return getRowsAsHashMap(a,countRows());
	}
	
	public ArrayList<HashMap<String,Object>> getRowsAsHashMap(Integer a, Integer b)
	{
		if(a<0)
		{
			a = countRows()+a;
		}
		
		if(b<0)
		{
			b = countRows()+b;
		}
		
		ArrayList<ArrayList<Object>> temp;
		
		if(a<b)
		{
			temp = new ArrayList<ArrayList<Object>>(content.subList(a,b));
		}
		else
		{
			temp = new ArrayList<ArrayList<Object>>(content.subList(b,a));
			
			Collections.reverse(temp);
		}
		
		ArrayList<HashMap<String,Object>> ret = new ArrayList<HashMap<String,Object>>();
		
		for(ArrayList<Object> row : temp)
		{
			HashMap<String,Object> hash_ret = new HashMap<String,Object>();
			
			for(a=0; a<row.size(); a++)
			{
				hash_ret.put(column_order.get(a),row.get(a));
			}
			
			ret.add(hash_ret);
		}
		
		return ret;
	}
	
	public int countRows()
	{
		return content.size();
	}
	
	public void setRawHeader(String header)
	{
		rawHeader = header;
	}
	
	public String getRawHeader()
	{
		return rawHeader;
	}
	
	public String setInfoField(String field, String value)
	{
		info_fields.put(field,value);
		
		return value;
	}
	
	public HashMap<String,String> getAllInfoFields()
	{
		return (HashMap<String,String>)info_fields.clone();
	}
	
	public ArrayList<String> getAllInfoFieldKeys()
	{
		ArrayList<String> fields = new ArrayList<String>();
		
		for(String key : info_fields.keySet())
		{
			fields.add(key);
		}
		
		return fields;
	}
	
	public String getInfoField(String field)
	{
		return info_fields.get(field);
	}
	
	public String getInfoField(String field, String default_value)
	{
		if(info_fields.containsKey(field))
		{
			return info_fields.get(field);
		}
		else
		{
			return default_value;
		}
	}
	
	public TableData sort(String field) throws ColumnNotFound
	{
		return sort(field,false);
	}
	
	public TableData sort(String field, Boolean reversed) throws ColumnNotFound
	{
		int index = column_order.indexOf(field);
		
		if(index < 0)
		{
			throw new ColumnNotFound(field);
		}
		
		ArrayList<RowSorting> list_to_sort = new ArrayList<RowSorting>();
		
		for(ArrayList<Object> row : content)
		{
			list_to_sort.add(new RowSorting(row,index));
		}
		
		Collections.sort(list_to_sort);
		
		for(index=0; index<list_to_sort.size(); index++)
		{
			content.set(index, list_to_sort.get(index).row);
		}
		
		if(reversed)
		{
			Collections.reverse(content);
		}
		
		return this;
	}
	
	public TableData filter(String field, FilterInterface filter) throws ColumnNotFound
	{
		ArrayList<ArrayList<Object>> new_content = new ArrayList<ArrayList<Object>>();
		
		int index = column_order.indexOf(field);
		
		if(index < 0)
		{
			throw new ColumnNotFound(field);
		}
		
		for(ArrayList<Object> row : content)
		{
			if(filter.isOk(row.get(index)))
			{
				new_content.add(row);
			}
		}
		
		content = new_content;
		
		return this;
	}
	
	public String toString()
	{
		ArrayList<Integer> max_lengths = new ArrayList<Integer>();
		ArrayList<ArrayList<String>> print = new ArrayList<ArrayList<String>>();
		int row=0;
		int value=0;
		
		for(String s : column_order)
		{
			max_lengths.add(s.length());
		}
		
		print.add((ArrayList<String>)column_order.clone());
		
		for(row=0; row<content.size(); row++)
		{
			ArrayList<String> temp = new ArrayList<String>();
			
			for(value=0; value<content.get(row).size(); value++)
			{
				String str = content.get(row).get(value).toString();
				
				temp.add(str);
				max_lengths.set(value,Math.max(max_lengths.get(value),str.length()));
			}
			
			print.add(temp);
		}
		
		int total_length = -1;
		
		for(value=0; value<max_lengths.size(); value++)
		{
			total_length += max_lengths.get(value) + 1;
		}
		
		String ret = rawHeader + String.format("\n\n%" + total_length + "s\n\n","").replace(' ','=');
		
		for(String s : getAllInfoFieldKeys())
		{
			ret = ret + String.format("%s: %s\n",s,getInfoField(s,""));
		}
		
		ret = ret + String.format("\n%" + total_length + "s\n\n","").replace(' ','=');
		
		for(row=0; row<print.size(); row++)
		{
			for(value=0; value<print.get(row).size(); value++)
			{
				if(value>0) ret = ret + "|";
				
				ret = ret + String.format("%"  + max_lengths.get(value) + "s",print.get(row).get(value));
			}
			
			if(row == 0)
			{
				ret = ret + String.format("\n%"  + total_length + "s","").replace(' ','-');
			}
			
			ret = ret + "\n";
		}
		
		return ret;
	}
	
	public Object clone()
	{
		TableData table = new TableData();
		
		table.setRawHeader(getRawHeader());
		
		for(String s : getAllInfoFieldKeys())
		{
			table.setInfoField(s,getInfoField(s,""));
		}
		
		ArrayList<String> columns = getColumns();
		HashMap<String,Class> types = getTypes();
		
		for(String s : columns)
		{
			try
			{
				table.addColumn(s,types.get(s));
			}
			catch(Exception e)
			{
				
			}
		}
		
		for(ArrayList<Object> row : getRows())
		{
			try
			{
				table.addRow(row);
			}
			catch(Exception e)
			{
				
			}
		}
		
		return table;
	}
	
	public static void main(String[] args)
	{
		TableData a = new TableData();
		
		a.setRawHeader("Cabecario teste\nNenhuma informacao aqui tem revelancia alguma\n\nEstacao = 213\nLatitude = 90N Longitude = 180W");
		
		a.setInfoField("Estacao","Vila Ventura");
		a.setInfoField("Latitude","90 Norte");
		a.setInfoField("Longitude","180 Oeste");
		
		try
		{
			a.addColumn("Name",String.class);
			a.addColumn("Ammount",Integer.class);
		}
		catch(ColumnAlreadyExists e)
		{
			out.println("Error 1");
		}
		
		ArrayList<Object> row = new ArrayList<Object>();
		row.add(new String("Batata"));
		row.add(new Integer(10));
		
		try
		{
			a.addRow(row);
		}
		catch(RowLengthMismatch e)
		{
			out.println("Error 2");
		}
		catch(TypeMismatch e)
		{
			out.println(e.received.toString());
			out.println(e.expected.toString());
		}
		
		row = new ArrayList<Object>();
		row.add(new String("Chinelo tosco"));
		row.add(new Integer(15));
		
		try
		{
			a.addRow(row);
		}
		catch(RowLengthMismatch e)
		{
			out.println("Error 2");
		}
		catch(TypeMismatch e)
		{
			out.println(e.received.toString());
			out.println(e.expected.toString());
		}
		
		row = new ArrayList<Object>();
		row.add(new String("Dente"));
		row.add(new Integer(3));
		
		try
		{
			a.addRow(row);
		}
		catch(RowLengthMismatch e)
		{
			out.println("Error 2");
		}
		catch(TypeMismatch e)
		{
			out.println(e.received.toString());
			out.println(e.expected.toString());
		}
		
		try
		{
			a.sort("Ammount",true);
		}
		catch(ColumnNotFound e)
		{
			out.println("Error 3");
		}
		
		out.println(a.clone());
		
		try
		{
			a.filter("Ammount",new CompareToFilter(
				new Integer(10),
				CompareToFilter.Comparison.LESS_THAN_OR_EQUAL
			));
		}
		catch(ColumnNotFound e)
		{
			out.println("Error 4");
		}
		
		out.println(a.clone());
		
		out.println(a.getRowsAsHashMap(0));
	}
}
