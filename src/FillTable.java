
/*
 * @author Leonardo
 * 
 * This class is instantiated on the mainWindow class at ActionListener method of the menu_arq_open component 
 * and it's responsible for populate the data table.
 * 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

import backend.RandomGeneratorParser;
import backend.TableData;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.table.DefaultTableModel;

/* Acho que a classe Parser deve receber o path do arquivo como parâmetro, sendo essa classe a ligação
* entre o front e o back, mas fica a cargo dos responsáveis pelo backend fazerem essas alterações, 
* no caso aqui ela está trabalhando com o parser de teste e depois terá
* que ser alterada para o Parser Genérico. -Deletar esse comentário-
*/
public class FillTable {
    
    private TableData dataToFill;
    private javax.swing.JTable table;

    private void populateTable() throws Exception{
        //String header = dataToFill.getRawHeader();
        ArrayList<Object> file_data;
        ArrayList<String> titles = dataToFill.getColumns();
        int number_titles = titles.size();
        int number_data_rows = dataToFill.countRows();
        
        int columns_table = table.getColumnCount();
        int rows_table = table.getRowCount();
        DefaultTableModel tableModel = (DefaultTableModel)table.getModel();
        
        //Inc columns and rows in JTable if necessary
        if (number_titles > columns_table){
            for(int i=0; i<(number_titles-columns_table); i++){
                tableModel.addColumn(null);
            }
            tableModel.fireTableStructureChanged();
        }
        
        if (number_data_rows > rows_table){
            for(int i=0; i<(number_data_rows-rows_table); i++){
                tableModel.addRow(new Object[number_titles]);
            }             
        }
        
        //update titles
        String[] new_table_ids= new String[number_titles];
        int aux=0;
        for(String title_column : titles){
            new_table_ids[aux]=title_column;
            aux++;
        } 
        tableModel.setColumnIdentifiers(new_table_ids);
        
        repopulateTable();
        
    }
    
    private void repopulateTable() throws Exception
    {
		ArrayList<Object> file_data;
        ArrayList<String> titles = dataToFill.getColumns();
        int number_titles = titles.size();
        int number_data_rows = dataToFill.countRows();
        DefaultTableModel tableModel = (DefaultTableModel)table.getModel();
		
		//populates data
        for(int i=0; i<number_data_rows; i++){
            file_data = dataToFill.getRow(i);
            for (int j=0; j<number_titles; j++){
                tableModel.setValueAt(file_data.get(j), i, j);
                
            }
        }
	}
    
    /*
     * @param  path   path of the file to be loaded
     * @param  table  reference to the Jtable of the GUI
     */
    public FillTable(TableData dataToFill, javax.swing.JTable table){ 
        this.dataToFill=dataToFill;
        this.table=table;
        
        try {
            populateTable();
        } catch (Exception ex) {
            Logger.getLogger(FillTable.class.getName()).log(Level.SEVERE, null, ex);
        }
   
    }
    
    public FillTable(TableData dataToFill, javax.swing.JTable table, boolean repopulating){ 
        this.dataToFill=dataToFill;
        this.table=table;
        
        try {
            if(repopulating)
            {
				repopulateTable();
			}
			else
			{
				populateTable();
			}
        } catch (Exception ex) {
            Logger.getLogger(FillTable.class.getName()).log(Level.SEVERE, null, ex);
        }
   
    }
    
}
